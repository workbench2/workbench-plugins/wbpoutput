from wbBase.application import App
from wbBase.applicationInfo import ApplicationInfo, PluginInfo
from wbBase.dialog.preferences import PreferencesDialog

appinfo = ApplicationInfo(Plugins=[PluginInfo(Name="output", Installation="default")])


def test_plugin():
    app = App(test=True, info=appinfo)
    assert "output" in app.pluginManager
    app.Destroy()


def test_OutputWin_instance():
    app = App(test=True, info=appinfo)
    from wbpOutput.outputwin import OutputWin

    outputWin = app.panelManager.getWindowByCaption("Output")
    assert isinstance(outputWin, OutputWin)
    app.Destroy()


def test_OutputWinPreferences_instance():
    app = App(test=True, info=appinfo)
    from wbpOutput.preferences import OutputWinPreferences

    preferencesDialog = PreferencesDialog(app.TopWindow)
    assert any(
        isinstance(c, OutputWinPreferences) for c in preferencesDialog.book.Children
    )
    app.Destroy()
