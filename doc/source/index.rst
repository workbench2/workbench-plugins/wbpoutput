
Welcome to the wbpOutput documentation
===============================================================================

wbpOutput is a plugin for `WorkBench <https://workbench2.gitlab.io/wbbase/>`_ 
applications. It adds a panel that shows everything that is normally printed 
to stdout or stderr.

.. figure:: _static/output.png
   :scale: 100 %
   :alt: Output Panel screenshot

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   wbpOutput

Indices and tables
-------------------------------------------------------------------------------

* :ref:`genindex`
* :ref:`modindex`
