wbpOutput package
=================

.. automodule:: wbpOutput
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

.. toctree::
   :maxdepth: 4

   wbpOutput.outputwin
   wbpOutput.preferences
